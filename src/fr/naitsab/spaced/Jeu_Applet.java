package fr.naitsab.spaced;

import java.applet.Applet;
import java.awt.BorderLayout;

public class Jeu_Applet extends Applet {
    private static final long serialVersionUID = 1L;

    private Affichage affichage = new Affichage();

    public void init() {
        setLayout(new BorderLayout());
        add(affichage);
    }

    public void start() {
        affichage.start();
    }

    public void stop() {
        affichage.stop();
    }
}
