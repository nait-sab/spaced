package fr.naitsab.spaced;


import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import javax.swing.JFrame;

import fr.naitsab.spaced.Entrees.Controlleur;
import fr.naitsab.spaced.Entrees.Entrees_Utilisateur;
import fr.naitsab.spaced.graphiques.*;

public class Affichage extends Canvas implements Runnable
{
    //Variables
    private static final long serialVersionUID = 1L;

    public static final int largeur = 800; //Fenêtre - taille x
    public static final int hauteur = 600; //Fenêtre - taille y
    public static final double version_jeu = 1.5;
    public static final String titre_jeu = "Spaced - version " + version_jeu;

    private Thread thread;
    private Ecran ecran;
    private BufferedImage image;
    private Entrees_Utilisateur entrees;
    private int new_x = 0;
    private int old_x = 0;
    private Jeu jeu;
    private Rendu rendu;
    private int fps;
    private int fps_display;
    private int[] pixels;
    private boolean en_cour = false;

    public Affichage()
    {
        Dimension taille = new Dimension(largeur, hauteur);
        setPreferredSize(taille);
        setMinimumSize(taille);
        setMaximumSize(taille);
        ecran = new Ecran(largeur, hauteur);
        jeu = new Jeu();
        image = new BufferedImage(largeur, hauteur, BufferedImage.TYPE_INT_RGB);
        entrees = new Entrees_Utilisateur();
        pixels = ((DataBufferInt)image.getRaster().getDataBuffer()).getData();

        addKeyListener(entrees);
        addFocusListener(entrees);
        addMouseListener(entrees);
        addMouseMotionListener(entrees);
    }

    public synchronized void start()
    {
        if (en_cour)
            return;
        en_cour = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop()
    {
        if (!en_cour)
            return;
        en_cour = false;
        try { thread.join(); } catch (Exception exception)
        {
            exception.printStackTrace();
            System.exit(0);
        }
    }

    public void run()
    {
        double secondes_ecouler = 0;
        long temps_precedent = System.nanoTime();
        double secondes_par_tic = 1 / 60.0;
        int tic_nombre = 0;
        boolean tic = false;

        while (en_cour)
        {
            long temps_actuel = System.nanoTime();
            long temps_passer = temps_actuel - temps_precedent;
            temps_precedent = temps_actuel;
            secondes_ecouler += temps_passer / 1000000000.0;
            requestFocus();

            while (secondes_ecouler > secondes_par_tic)
            {
                horloge();
                secondes_ecouler -= secondes_par_tic;
                tic = true;
                tic_nombre++;
                if (tic_nombre % 60 == 0)
                {
                    System.out.println(fps + " FPS");
                    temps_precedent += 1000;
                    fps_display = fps;
                    fps = 0;
                }
            }
            if (tic)
            {
                rendu();
                fps++;
            }
            rendu();
            fps++;
            new_x = Entrees_Utilisateur.mouse_x;
            if (new_x > old_x)
            {
                Controlleur.tourner_droite = true;
            }
            if (new_x < old_x)
            {
                Controlleur.tourner_gauche = true;
            }
            if (new_x == old_x)
            {
                Controlleur.tourner_gauche = false;
                Controlleur.tourner_droite = false;
            }
            old_x = new_x;

        }
    }

    private void horloge()
    {
        jeu.raccourcis(entrees.touche);
    }

    private void rendu()
    {
        BufferStrategy image_strategie = this.getBufferStrategy();
        if (image_strategie == null)
        {
            createBufferStrategy(3);
            return;
        }
        ecran.rendu(jeu);
        for (int compteur = 0; compteur < largeur * hauteur; compteur++)
        {
            pixels[compteur] = ecran.pixels[compteur];
        }
        Graphics graphiques = image_strategie.getDrawGraphics();
        graphiques.drawImage(image, 0, 0, largeur + 10, hauteur + 10, null);
        graphiques.setFont(new Font("Verdana", 3, 50));
        graphiques.setColor(Color.magenta);
        graphiques.drawString("FPS : " + fps_display, 20, 50);
        graphiques.dispose();
        image_strategie.show();
    }

    public static void main(String[] args)
    {
        System.out.println("Information : Initialisation du programme."); //DEBUG

        BufferedImage curseur = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor blanc = Toolkit.getDefaultToolkit().createCustomCursor(curseur, new Point(0, 0), "blanc");
        Affichage jeu = new Affichage();
        JFrame frame = new JFrame();
        frame.add(jeu);
        frame.pack();
        frame.getContentPane().setCursor(blanc);
        frame.setTitle(titre_jeu); //Définir le titre de la fenêtre
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Arrêter le programme à la fermeture de la fenêtre
        frame.setLocationRelativeTo(null); //Définir la position de la fenêtre
        frame.setResizable(false); //Empêcher de modifier la taille de la fenêtre
        frame.setVisible(true); //Rendre la fenêtre visible

        System.out.println("Information : Programme en cour..."); //DEBUG

        jeu.start();
    }
}
