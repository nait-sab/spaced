package fr.naitsab.spaced;

import fr.naitsab.spaced.Entrees.Controlleur;

import java.awt.event.KeyEvent;

public class Jeu
{
    public int temps;
    public Controlleur controles;

    public Jeu()
    {
        controles = new Controlleur();
    }

    public void raccourcis(boolean[] touche)
    {
        temps++;
        boolean deplacement_avant = touche[KeyEvent.VK_Z];
        boolean deplacement_bas = touche[KeyEvent.VK_S];
        boolean deplacement_gauche = touche[KeyEvent.VK_Q];
        boolean deplacement_droite = touche[KeyEvent.VK_D];
        boolean sauter = touche[KeyEvent.VK_SPACE];
        boolean accroupi = touche[KeyEvent.VK_CONTROL];
        boolean courrir = touche[KeyEvent.VK_SHIFT];

        controles.mouvements(
                deplacement_avant, deplacement_bas, deplacement_gauche,
                deplacement_droite, sauter, accroupi, courrir
        );
    }
}
