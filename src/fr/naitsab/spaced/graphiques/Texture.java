package fr.naitsab.spaced.graphiques;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;

public class Texture
{
    public static Rendu sol = charger_image("ressources/textures/sol.png");

    public static Rendu charger_image(String nom_fichier)
    {
        try {
            BufferedImage image = ImageIO.read(Texture.class.getResource(nom_fichier));
            int largeur = image.getWidth();
            int hauteur = image.getHeight();
            Rendu resultat = new Rendu(largeur, hauteur);
            image.getRGB(0, 0, largeur, hauteur, resultat.pixels, 0, largeur);
            return resultat;
        } catch (Exception exception) {
            System.out.println("Erreur::Texture::Impossible de charger la texture du sol.");
            throw new RuntimeException(exception);
        }
    }
}
