package fr.naitsab.spaced.graphiques;

import fr.naitsab.spaced.Entrees.Controlleur;
import fr.naitsab.spaced.Jeu;

public class Rendu3D extends Rendu
{
    //Variables
    public double[] tampon_z;
    private double distance_rendu = 5000; //Distance luminosité

    //Constructeur
    public Rendu3D(int largeur, int hauteur) {
        super(largeur, hauteur);
        tampon_z = new double[largeur * hauteur];
    }

    //Fonction de modélisation
    public void sol(Jeu jeu)
    {
        //Qualité pixels rendu
        double sol_position = 8;
        double plafond_position = 45;

        //Déplacement
        double deplacement_avant = jeu.controles.y;
        double deplacement_droite = jeu.controles.x;
        double saut = jeu.controles.z;

        //Animation de déplacement
        double marchant;
        if (jeu.controles.get_anim_accroupi()) {
            marchant = Math.sin(jeu.temps / 0.6) * 0.25;
        }
        else if (jeu.controles.get_anim_courrir()) {
            marchant = Math.sin(jeu.temps / 0.6) * 0.8;
        }
        else {
            marchant = Math.sin(jeu.temps / 6.0) * 0.5;
        }

        //Création rotation
        double rotation = jeu.controles.get_rotation_z();
        double rotation_cos = Math.cos(rotation);
        double rotation_sin = Math.sin(rotation);


        for (int y = 0; y < hauteur; y++)
        {
            double plafond = (y + -hauteur / 2.0) / hauteur;

            double z = (sol_position + saut) / plafond;
            if (jeu.controles.get_anim_marche()) {
                z = (sol_position + saut + marchant) / plafond;
            }

            if (plafond < 0)
            {
                z = (plafond_position - saut) / -plafond;
                if (jeu.controles.get_anim_marche()) {
                    z = (plafond_position - saut -marchant) / -plafond;
                }
            }

            for (int x = 0; x < largeur; x++)
            {
                double profondeur = (x - largeur / 2.0) / hauteur;
                profondeur *= z;
                double xx = profondeur * rotation_cos + z * rotation_sin;
                double yy = z * rotation_cos - profondeur * rotation_sin;
                int x_pixel = (int)(xx + deplacement_droite);
                int y_pixel = (int)(yy + deplacement_avant);
                tampon_z[x + y * largeur] = z;
                pixels[x + y * largeur] = Texture.sol.pixels[(x_pixel & 7) + (y_pixel & 7) * 8];

                // Chargement version texture : Texture.sol.pixels[(x_pixel & 7) + (y_pixel & 7) * 8];

                //DEBUG :: Affichage sans texture
                //((x_pixel & 15) * 16) | ((y_pixel & 15) * 16) << 8;

                //Arrête de l'affichage pixels au centre du sol et du plafond
                if (z > 500)
                {
                    pixels[x + y * largeur] = 0;
                }
            }
        }
    }

    public void rendu_limite_distance()
    {
        //Luminosité pour fondre le rendu distant
        for (int compteur = 0; compteur < largeur * hauteur; compteur++)
        {
            int couleur = pixels[compteur];
            int luminosite = (int)(distance_rendu / (tampon_z[compteur]));

            if (luminosite < 0)
            {
                luminosite = 0;
            }
            if (luminosite > 255)
            {
                luminosite = 255;
            }

            int r = (couleur >> 16) & 0xff;
            int g = (couleur >> 8) & 0xff;
            int b = (couleur) & 0xff;

            r = r * luminosite / 255;
            g = g * luminosite / 255;
            b = b * luminosite / 255;

            pixels[compteur] = r << 16 | g << 8 | b;
        }
    }
}
