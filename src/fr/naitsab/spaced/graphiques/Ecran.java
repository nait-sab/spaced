package fr.naitsab.spaced.graphiques;

import fr.naitsab.spaced.Jeu;

import java.util.Random;

public class Ecran extends Rendu
{
    private Rendu test;
    private Rendu3D rendu;

    public Ecran(int largeur, int hauteur)
    {
        super(largeur, hauteur);
        Random aleatoire = new Random();
        rendu = new Rendu3D(largeur, hauteur);
        test = new Rendu(256, 256);
        for (int compteur = 0; compteur < 256 * 256; compteur++)
        {
            test.pixels[compteur] = aleatoire.nextInt() * (aleatoire.nextInt(5) / 4);
        }
    }

    public void rendu(Jeu jeu)
    {
        for (int compteur = 0; compteur < largeur * hauteur; compteur++)
        {
            pixels[compteur] = 0;
        }

        rendu.sol(jeu);
        rendu.rendu_limite_distance();
        dessiner(rendu, 0, 0);
    }
}
