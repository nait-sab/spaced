package fr.naitsab.spaced.graphiques;

public class Rendu
{
    public final int largeur;
    public final int hauteur;
    public final int[] pixels;

    public Rendu(int largeur, int hauteur)
    {
        this.largeur = largeur;
        this.hauteur = hauteur;
        pixels = new int[largeur * hauteur];
    }

    public void dessiner(Rendu rendu, int x_position, int y_position)
    {
        for (int y = 0; y < rendu.hauteur; y++)
        {
            int y_pixel = y + y_position;
            if (y_pixel < 0 || y_pixel >= hauteur) continue;

            for (int x = 0; x < rendu.largeur; x++)
            {
                int x_pixel = x + x_position;
                if (x_pixel < 0 || x_pixel >= largeur) continue;

                int alpha = rendu.pixels[x + y * rendu.largeur];
                if (alpha > 0)
                {
                    pixels[x_pixel + y_pixel * largeur] = alpha;
                }
            }
        }
    }
}
