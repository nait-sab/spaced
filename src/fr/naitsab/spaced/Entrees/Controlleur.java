package fr.naitsab.spaced.Entrees;

public class Controlleur
{
    //Variables
    public double x;
    public double y;
    public double z;

    public double axe_x;
    public double axe_y;
    public double axe_z;

    public double rotation_z;
    public static boolean tourner_gauche = false;
    public static boolean tourner_droite = false;

    //Animations
    public static boolean anim_marche = false;
    public static boolean anim_accroupi = false;
    public static boolean anim_courrir = false;

    public void mouvements(boolean avant, boolean bas, boolean gauche, boolean droite,
                        boolean sauter, boolean accroupi, boolean courrir)
    {
        //Variables de la vitesse lors des mouvements
        double vitesse_rotation = 0.025;
        double vitesse_marche = 0.5;
        double saut_hauteur = 0.5;
        double accroupi_hauteur = 0.3;

        //Variables de calculs pour les mouvements
        double mouvement_x = 0;
        double mouvement_y = 0;

        //Gestion des entrées clavier en boolean pour activer les mouvements correspondant
        if (avant) {
            mouvement_y++;
            anim_marche = true;
        }

        if (bas) {
            mouvement_y--;
            anim_marche = true;
        }

        if (gauche) {
            mouvement_x--;
            anim_marche = true;
        }

        if (droite) {
            mouvement_x++;
            anim_marche = true;
        }

        if (tourner_gauche) {
            axe_z -= vitesse_rotation;
            anim_marche = true;
        }

        if (tourner_droite) {
            axe_z += vitesse_rotation++;
            anim_marche = true;
        }

        if (!avant && !bas && !droite && !gauche && !tourner_droite && !tourner_gauche) {
            anim_marche = false;
        }

        if (sauter) {
            z += saut_hauteur;
            courrir = false;
        }

        if (accroupi) {
            z -= accroupi_hauteur;
            courrir = false;
            anim_accroupi = true;
            vitesse_marche = 0.2;
        } else { anim_accroupi = false; }

        if (courrir) {
            vitesse_marche = 1.0;
            anim_courrir = true;
        } else { anim_courrir = false; }

        axe_x = (mouvement_x * Math.cos(rotation_z) + mouvement_y * Math.sin(rotation_z)) * vitesse_marche;
        axe_y = (mouvement_y * Math.cos(rotation_z) - mouvement_x * Math.sin(rotation_z)) * vitesse_marche;

        x += axe_x;
        y += axe_y;
        z *= 0.9;
        axe_x *= 0.1;
        axe_y *= 0.1;
        axe_z *= 0.5;
        rotation_z += axe_z;
    }

    //Accesseurs
    public double get_rotation_z() { return rotation_z; }
    public boolean get_anim_marche() { return anim_marche; }
    public boolean get_anim_accroupi() { return anim_accroupi; }
    public boolean get_anim_courrir() { return anim_courrir; }
}
