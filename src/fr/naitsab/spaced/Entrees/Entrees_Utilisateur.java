package fr.naitsab.spaced.Entrees;

import java.awt.event.*;

public class Entrees_Utilisateur implements KeyListener, FocusListener, MouseListener, MouseMotionListener
{
    public boolean[] touche = new boolean[68836];
    public static int mouse_x;
    public static int mouse_y;

    public void mouseDragged(MouseEvent evenement) {}
    public void mouseMoved(MouseEvent evenement)
    {
        mouse_x = evenement.getX();
        mouse_y = evenement.getY();
    }
    public void mouseClicked(MouseEvent evenement) {}
    public void mouseEntered(MouseEvent evenement) {}
    public void mouseExited(MouseEvent evenement) {}
    public void mousePressed(MouseEvent evenement) {}
    public void mouseReleased(MouseEvent evenement) {}

    public void focusGained(FocusEvent evenement) {}
    public void focusLost(FocusEvent evenement)
    {
        for (int compteur = 0; compteur < touche.length; compteur++)
        {
            touche[compteur] = false;
        }
    }

    public void keyPressed(KeyEvent evenement)
    {
        //Lorsque la touche est enfoncée
        int touche_code = evenement.getKeyCode();
        if (touche_code > 0 && touche_code < touche.length)
        {
            touche[touche_code] = true;
        }
    }
    public void keyReleased(KeyEvent evenement)
    {
        //Lorsque la touche est relâchée
        int touche_code = evenement.getKeyCode();
        if (touche_code > 0 && touche_code < touche.length)
        {
            touche[touche_code] = false;
        }
    }
    public void keyTyped(KeyEvent evenement) {}
}
